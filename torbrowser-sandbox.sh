#!/bin/bash

#   Forensics Resistant Tor Browser Sandbox
#
#   Copyright (C) 2019 madaidan
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Easy configuration.
disable_apparmor="0"
enable_swap="0"
cold_boot_protection="0"
self_destruct="0"
safest_level="0"
save_bash_history="0"
preserve="0"
x11_sandbox="0"
firewall="0"
reenable_ufw="0"

# Tor Browser version.
tbb_ver="9.0.1"

# Bubblewrap sandbox.
bwrap_sandbox="bwrap \
--ro-bind /usr/bin/chroot /usr/bin/chroot \
--ro-bind /usr/lib /usr/lib \
--symlink /usr/lib /lib \
--symlink /usr/lib /lib64 \
--dev-bind /tmp/torbrowser-sandbox /tmp/torbrowser-sandbox \
--ro-bind /tmp/torbrowser-sandbox/etc/ /tmp/torbrowser-sandbox/etc/ \
--ro-bind /tmp/torbrowser-sandbox/var/ /tmp/torbrowser-sandbox/var/ \
--ro-bind /tmp/torbrowser-sandbox/usr/ /tmp/torbrowser-sandbox/usr/ \
--ro-bind /tmp/torbrowser-sandbox/bin/ /tmp/torbrowser-sandbox/bin/ \
--ro-bind /tmp/torbrowser-sandbox/lib/ /tmp/torbrowser-sandbox/lib/ \
--ro-bind /tmp/torbrowser-sandbox/lib64/ /tmp/torbrowser-sandbox/lib64/ \
--ro-bind /tmp/torbrowser-sandbox/run/ /tmp/torbrowser-sandbox/run/ \
--ro-bind /tmp/torbrowser-sandbox/mnt/ /tmp/torbrowser-sandbox/mnt/ \
--ro-bind /tmp/torbrowser-sandbox/boot/ /tmp/torbrowser-sandbox/boot/ \
--ro-bind /tmp/torbrowser-sandbox/proc/ /tmp/torbrowser-sandbox/proc/ \
--ro-bind /tmp/torbrowser-sandbox/srv/ /tmp/torbrowser-sandbox/srv/ \
--ro-bind /tmp/torbrowser-sandbox/tmp/ /tmp/torbrowser-sandbox/tmp/ \
--ro-bind /tmp/torbrowser-sandbox/root/ /tmp/torbrowser-sandbox/root/ \
--ro-bind /tmp/torbrowser-sandbox/sys/ /tmp/torbrowser-sandbox/sys/ \
--ro-bind /tmp/torbrowser-sandbox/opt/ /tmp/torbrowser-sandbox/opt/ \
--ro-bind /tmp/torbrowser-sandbox/home/ /tmp/torbrowser-sandbox/home/ \
--bind /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/ /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/ \
--unshare-pid \
--unshare-uts \
--hostname host \
--unshare-cgroup \
--unshare-ipc \
--proc /tmp/torbrowser-sandbox/proc/ \
--tmpfs /tmp/torbrowser-sandbox/tmp/ \
--dev /dev/ \
--dev /tmp/torbrowser-sandbox/dev/ /tmp/torbrowser-sandbox/dev/ \
--new-session \
--die-with-parent \
--as-pid-1 \
--unsetenv XAUTHORITY \
--unsetenv SUDO_COMMAND \
--unsetenv SUDO_USER \
--unsetenv SUDO_UID \
--unsetenv SUDO_GID \
--setenv DISPLAY ${RANDOM} \
--setenv HOME /home/user \
--setenv SHELL /bin/false \
--setenv USER user \
--setenv LOGNAME user \
--setenv LANG en_US.UTF-8 \
--cap-drop all \
--cap-add CAP_SYS_CHROOT \
--cap-add CAP_SETUID \
--cap-add CAP_SETGID \
"

bwrap_x11_sandbox="bwrap --dev-bind / / \
--ro-bind /etc/ /etc/ \
--ro-bind /var/ /var/ \
--ro-bind /usr/ /usr/ \
--ro-bind /bin/ /bin/ \
--ro-bind /lib/ /lib/ \
--ro-bind /lib64/ /lib64/ \
--ro-bind /run/ /run/ \
--ro-bind /mnt/ /mnt/ \
--ro-bind /boot/ /boot/ \
--ro-bind /proc/ /proc/ \
--ro-bind /srv/ /srv/ \
--ro-bind /tmp/ /tmp/ \
--ro-bind /root/ /root/ \
--ro-bind /sys/ /sys/ \
--ro-bind /opt/ /opt/ \
--ro-bind /home/ /home/ \
--unshare-pid \
--unshare-uts \
--unshare-cgroup \
--unshare-ipc \
--proc /proc/ \
--tmpfs /tmp/ \
--dev /dev/ \
--new-session \
--die-with-parent \
--as-pid-1 \
--setenv SHELL /bin/false \
--cap-drop all \
"

# Apparmor Profiles.
apparmor_profiles="/etc/apparmor.d/torbrowser-sandbox.Tor.tor /etc/apparmor.d/torbrowser-sandbox.firefox.real /etc/apparmor.d/tunables/global-sandbox /etc/apparmor.d/abstractions/base-sandbox /etc/apparmor.d/abstractions/dri-common-sandbox /etc/apparmor.d/abstractions/fonts-sandbox /etc/apparmor.d/abstractions/freedesktop.org-sandbox /etc/apparmor.d/abstractions/gnome-sandbox /etc/apparmor.d/abstractions/wayland-sandbox /etc/apparmor.d/tunables/torbrowser-sandbox"

# Secure file downloading.
# https://github.com/Whonix/scurl
scurl="curl --tlsv1.2 --proto =https --location --remote-name-all --remote-header-name"

# Sets variables for fancy colours.
red=$'\e[0;91m'
green=$'\e[0;92m'
endc=$'\e[0m'

# Xephyr's DISPLAY variable.
xephyr_display=":10"

# Creates firewall.
create_firewall() {
  # Disable UFW if it's enabled. UFW may cause problems with iptables.
  if [ -x "$(command -v ufw)" ]; then
    if sudo ufw status | grep "Status: active" >/dev/null; then
      sudo ufw disable &>/dev/null
      reenable_ufw=1
    fi
  fi

  # Install ipset.
  if ! pacman -Qq ipset &>/dev/null; then
    sudo pacman -S --noconfirm -q ipset >/dev/null
    remove_ipset=1
  fi

  # Load required modules if not already.
  if ! lsmod | grep "ip_tables" >/dev/null; then
    sudo modprobe ip_tables iptable_nat ip_conntrack iptable-filter ipt_state
  fi

  # Enables iptables if not already.
  if ! systemctl is-active iptables.service >/dev/null 2>&1; then
    sudo systemctl start iptables.service
  fi
  if [ -f /proc/net/if_inet6 ]; then
    if ! systemctl is-active ip6tables.service >/dev/null 2>&1; then
      sudo systemctl start ip6tables.service
    fi
  fi

  # Save current iptables rules.
  sudo iptables-save | sudo tee /tmp/iptables-rules
  if [ -f /proc/net/if_inet6 ]; then
    sudo ip6tables-save | sudo tee /tmp/ip6tables-rules
  fi

  # Reset iptables.
  sudo iptables -F
  sudo iptables -X
  sudo iptables -t nat -F
  sudo iptables -t nat -X
  sudo iptables -t mangle -F
  sudo iptables -t mangle -X
  sudo iptables -t raw -F
  sudo iptables -t raw -X
  sudo iptables -t security -F
  sudo iptables -t security -X

  # Reset ip6tables.
  if [ -f /proc/net/if_inet6 ]; then
    sudo ip6tables -F
    sudo ip6tables -X
    sudo ip6tables -t nat -F
    sudo ip6tables -t nat -X
    sudo ip6tables -t mangle -F
    sudo ip6tables -t mangle -X
    sudo ip6tables -t raw -F
    sudo ip6tables -t raw -X
    sudo ip6tables -t security -F
    sudo ip6tables -t security -X
  fi

  # Drop all incoming, output and forwarding IPv4 traffic.
  sudo iptables -P INPUT DROP
  sudo iptables -P OUTPUT DROP
  sudo iptables -P FORWARD DROP

  # Drop all incoming, output and forwarding IPv6 traffic if IPv6 is enabled.
  if [ -f /proc/net/if_inet6 ]; then
    sudo ip6tables -P INPUT DROP
    sudo ip6tables -P OUTPUT DROP
    sudo ip6tables -P FORWARD DROP
  fi

  # Accept incoming traffic related to an established connection.
  sudo iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

  # Allow incoming traffic on the loopback interface.
  sudo iptables -A INPUT -i lo -j ACCEPT

  # Accept outgoing traffic related to an established connection.
  sudo iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

  # Allow outgoing traffic on the loopback interface.
  sudo iptables -A OUTPUT -o lo -j ACCEPT

  # Allow Tor traffic using ipset.
  if sudo ipset -L | grep "tor-nodes"; then
    # Delete "tor-nodes" ipset if it exists.
    sudo ipset destroy tor-nodes
  fi

  # Create ipset and add lists of all guard nodes.
  sudo ipset create tor-nodes hash:ip
  for ip in $(grep -B2 "^s.*Guard" "${HOME}"/tor-browser_en-US/Browser/TorBrowser/Data/Tor/cached-microdesc-consensus | grep "^r" | awk '{print $6}');
    do sudo ipset -A tor-nodes "$ip";
  done

  # Add directory authorities.
  sudo ipset -A tor-nodes 194.109.206.212
  sudo ipset -A tor-nodes 66.111.2.131
  sudo ipset -A tor-nodes 128.31.0.34
  sudo ipset -A tor-nodes 86.59.21.38
  sudo ipset -A tor-nodes 204.13.164.118
  sudo ipset -A tor-nodes 171.25.193.9
  sudo ipset -A tor-nodes 193.23.244.244
  sudo ipset -A tor-nodes 154.35.175.225
  sudo ipset -A tor-nodes 131.188.40.189
  sudo ipset -A tor-nodes 199.58.81.140

  # Accept all traffic to and from the IPs in the ipset.
  sudo iptables -A OUTPUT -m set --match-set tor-nodes src -j ACCEPT
  sudo iptables -A INPUT -m set --match-set tor-nodes src -j ACCEPT

  # Block all other traffic.
  sudo iptables -A OUTPUT -j REJECT

  # Logging rules.
  sudo iptables -N logdrop
  sudo iptables -A logdrop -m limit --limit 5/m --limit-burst 10 -j LOG
  sudo iptables -A logdrop -j DROP
  sudo iptables -A INPUT -j logdrop
  sudo iptables -A OUTPUT -j logdrop

}

## Flags.
while test $# -gt 0; do
        case "$1" in
                --disable-apparmor)
                                 echo "AppArmor will now be disabled."
                                 disable_apparmor=1
                                 break
                                 ;;
                --enable-swap)
                                 echo "Swap will stay enabled."
                                 enable_swap=1
                                 break
                                 ;;
                --cold-boot-protection)
                                 echo "Cold boot attack protection is now enabled."
                                 cold_boot_protection=1
                                 break
                                 ;;
                --self-destruct)
                                 echo "This script will self-destruct once it's finished, leaving no traces."
                                 self_destruct=1
                                 break
                                 ;;
                --safest-level)
                                 echo "The Tor Browser will start with the safest security level."
                                 safest_level=1
                                 break
                                 ;;
                --save-bash-history)
                                 echo "Bash history will not be cleared."
                                 save_bash_history=1
                                 break
                                 ;;
                -s | --start-tor-browser)
                                 if [ -d /tmp/torbrowser-sandbox/ ]; then
                                   # Kills any open Tor Browser before running.
                                   kill_tor_browser

                                   # Disables swap.
                                   if ! sudo grep "1" /tmp/torbrowser-sandbox/root/enable_swap &>/dev/null; then
                                     # Gets vm.swappiness value and saves it as a variable. Used for re-enabling swap.
                                     swappiness_value=$(sysctl vm.swappiness | awk '{print $3}')

                                     # Makes the kernel only swap if absolutely necessary.
                                     sudo sysctl vm.swappiness=1 >/dev/null

                                     # Checks if swap is enabled. If so, is re-enabled later in the script. This is to prevent enabling an unwanted swap partition.
                                     if free | awk '/^Swap:/ {exit !$2}'; then
                                       enable_swap_partitions=1
                                     fi

                                     # Disables swap completely.
                                     #sudo swapoff -a
                                   fi

                                   # Starts X11 sandbox if needed.
                                   if sudo grep "1" /tmp/torbrowser-sandbox/root/x11 &>/dev/null; then
                                     # Starts Xephyr.
                                     ${bwrap_x11_sandbox} /usr/bin/Xephyr -ac "${xephyr_display}" &>/dev/null &

                                     # Changes resolution of Xephyr window to 1024x768.
                                     sleep 1
                                     xrandr --display "${xephyr_display}" --output default --mode 1024x768 &>/dev/null
                                   fi

                                   # Creates firewall if needed.
                                   if sudo grep "1" /tmp/torbrowser-sandbox/root/firewall &>/dev/null; then
                                     create_firewall
                                   fi

                                   # Starts Tor Browser.
                                   printf "\\n${green}%s${endc}\\n" \
                                          "Tor Browser is running..."

                                   sudo ${bwrap_sandbox} chroot --userspec=user /tmp/torbrowser-sandbox/ /script.sh

                                   printf "\\n${green}%s${endc}\\n" \
                                          "Tor Browser has exited."

                                   # Makes sure Tor Browser is killed after.
                                   kill_tor_browser

                                   ## Restore iptables rules.
                                   if sudo grep "1" /tmp/torbrowser-sandbox/root/firewall &>/dev/null; then
                                     sudo iptables-restore < /tmp/iptables-rules
                                     if [ -f /proc/net/if_inet6 ]; then
                                       sudo ip6tables-restore < /tmp/ip6tables-rules
                                     fi

                                     # Delete /tmp/iptables-rules.
                                     sudo shred -zu /tmp/iptables-rules

                                     # Re-enable UFW if needed.
                                     if [ "${reenable_ufw}" = "1" ]; then
                                       sudo ufw enable >/dev/null
                                     fi
                                   fi

                                   ## Re-enable swapping.
                                   if ! sudo grep "1" /tmp/torbrowser-sandbox/root/enable_swap &>/dev/null; then
                                     # Sets vm.swappiness value back to default.
                                     sudo sysctl vm.swappiness="${swappiness_value}" >/dev/null

                                     # Enables swap partitions/files again.
                                     #if [ "${enable_swap_partitions}" = "1" ]; then
                                     #  sudo swapon -a
                                     #fi
                                   fi
                                 else
                                   printf "\\n${red}%s${endc}\\n" \
                                          "WARNING: THE SANDBOX HAS NOT BEEN CREATED. EXITING."
                                 fi
                                 exit 1
                                 ;;
                -p | --preserve)
                                 echo "The Tor Browser sandbox will not be removed and no directories will be unmounted at the end of the script."
                                 preserve=1
                                 break
                                 ;;
                --x11-sandbox)
                                 echo "The Tor Browser will now run in an X11 sandbox."
                                 x11_sandbox=1
                                 break
                                 ;;
                --firewall)
                                 #echo "The firewall will now be created to prevent clearnet leaks."
                                 #firewall=1
                                 echo "The firewall is currently broken."
                                 exit 1
                                 #break
                                 ;;
                -d | --debug)
                                 if [ -d /tmp/torbrowser-sandbox/ ]; then
                                   # Remove unnessecary capabilities and set default shell. SETUID and SETGID are only needed for the --userspec option for the chroot command.
                                   bwrap_sandbox="${bwrap_sandbox}
                                   --cap-drop CAP_SETGID \
                                   --cap-drop CAP_SETUID \
                                   --setenv SHELL /bin/bash \
                                   "

                                   # Chroot into sandbox as root.
                                   sudo ${bwrap_sandbox} chroot /tmp/torbrowser-sandbox/
                                 else
                                   printf "\\n${red}%s${endc}\\n" \
                                          "WARNING: THE SANDBOX HAS NOT BEEN CREATED. EXITING."
                                 fi
                                 exit 1
                                 ;;
                -du | --debug-user)
                                 if [ -d /tmp/torbrowser-sandbox/ ]; then
                                   # Change shell variable.
                                   bwrap_sandbox="${bwrap_sandbox}
                                   --setenv SHELL /bin/bash
                                   "
                                   # Chroot into sandbox as a user.
                                   sudo ${bwrap_sandbox} chroot --userspec=user /tmp/torbrowser-sandbox/
                                 else
                                   printf "\\n${red}%s${endc}\\n" \
                                          "WARNING: THE SANDBOX HAS NOT BEEN CREATED. EXITING."
                                 fi
                                 exit 1
                                 ;;
                --cleanup)
                                 # Deletes directories.
                                 if [ -d /tmp/torbrowser-sandbox ]; then
                                   sudo rm -rf /tmp/torbrowser-sandbox/
                                 fi

                                 if [ -d /tmp/sandboxed-tor-browser ]; then
                                   sudo rm -rf /tmp/sandboxed-tor-browser/
                                 fi

                                 # Remove AppArmor definitions.
                                 if [ -f /etc/apparmor.d/torbrowser-sandbox.Tor.tor ]; then
                                   sudo apparmor_parser -R /etc/apparmor.d/torbrowser-sandbox.Tor.tor /etc/apparmor.d/torbrowser-sandbox.firefox.real

                                   # Shred AppArmor profiles.
                                   sudo shred -zu ${apparmor_profiles}
                                 fi

                                 # Remove Xephyr AppArmor profile.
                                 if [ -f /etc/apparmor.d/usr.bin.Xephyr-sandbox ]; then
                                   sudo apparmor_parser -R /etc/apparmor.d/usr.bin.Xephyr-sandbox
                                   sudo shred -zu /etc/apparmor.d/usr.bin.Xephyr-sandbox

                                   # Remove Openbox AppArmor profile.
                                   sudo apparmor_parser -R /etc/apparmor.d/torbrowser-x11-sandbox-openbox &>/dev/null # The output is sent to /dev/null because it gets an error about tunables/global-sandbox not existing otherwise.
                                   sudo shred -zu /etc/apparmor.d/torbrowser-x11-sandbox-openbox
                                 fi

                                 exit 1
                                 ;;
                -m | --multiple-options)
                                 read -r -p "" options
                                 if echo "${options}" | grep "disable-apparmor" >/dev/null; then
                                   echo "AppArmor will now be disabled."
                                   disable_apparmor=1
                                 fi

                                 if echo "${options}" | grep "enable-swap" >/dev/null; then
                                   echo "Swap will stay enabled."
                                   enable_swap=1
                                 fi

                                 if echo "${options}" | grep "cold-boot-protection" >/dev/null; then
                                   if echo "${options}" | grep "preserve" >/dev/null; then
                                     # Cold boot attack protection and preserve cannot be used together.
                                     printf "\\n${red}%s${endc}\\n" \
                                            "WARNING: COLD BOOT ATTACK PROTECTION AND PRESERVE CANNOT BE USED TOGETHER."
                                     exit 1
                                   fi
                                   echo "Cold boot attack protection is now enabled."
                                   cold_boot_protection=1
                                 fi

                                 if echo "${options}" | grep "self-destruct" >/dev/null; then
                                   if echo "${options}" | grep "preserve" >/dev/null; then
                                     # Self destruct and preserve cannot be used together.
                                     printf "\\n${red}%s${endc}\\n" \
                                            "WARNING: SELF DESTRUCT AND PRESERVE CANNOT BE USED TOGETHER."
                                     exit 1
                                   fi
                                   echo "This script will self-destruct once it's finished, leaving no traces."
                                   self_destruct=1
                                 fi

                                 if echo "${options}" | grep "safest-level" >/dev/null; then
                                   echo "The Tor Browser will start with the safest security level."
                                   safest_level=1
                                 fi

                                 if echo "${options}" | grep "save-bash-history" >/dev/null; then
                                   echo "Bash history will not be cleared."
                                   save_bash_history=1
                                 fi

                                 if echo "${options}" | grep "start-tor-browser" >/dev/null; then
                                   printf "\\n${red}%s${endc}\\n" \
                                          "WARNING: START TOR BROWSER CANNOT BE USED WITH ANOTHER FLAG."
                                   exit 1
                                 fi

                                 if echo "${options}" | grep "x11-sandbox" >/dev/null; then
                                   echo "The Tor Browser will now run in an X11 sandbox."
                                   x11_sandbox=1
                                 fi

                                 if echo "${options}" | grep "firewall" >/dev/null; then
                                   echo "The firewall will now be created to prevent clearnet leaks."
                                   firewall=1
                                 fi

                                 if echo "${options}" | grep "debug" >/dev/null; then
                                   printf "\\n${red}%s${endc}\\n" \
                                          "WARNING: DEBUG OPTIONS CANNOT BE USED WITH ANOTHER FLAG."
                                   exit 1
                                 fi

                                 if echo "${options}" | grep "cleanup" >/dev/null; then
                                   printf "\\n${red}%s${endc}\\n" \
                                          "WARNING: CLEANUP CANNOT BE USED WITH ANOTHER FLAG."
                                   exit 1
                                 fi

                                 if echo "${options}" | grep "preserve" >/dev/null; then
                                   echo "The Tor Browser sandbox will not be removed and no directories will be unmounted at the end of the script."
                                   preserve=1
                                 fi
                                 break
                                 ;;

                -h | --help)
                                 echo "Forensics Resistant Tor Browser Sandbox"
                                 echo ""
                                 echo "Usage: ./torbrowser-sandbox [options]"
                                 echo ""
                                 echo "Options:"
                                 echo ""
                                 echo "    --disable-apparmor                Disables Apparmor."
                                 echo "    --enable-swap                     Enables swap."
                                 echo "    --cold-boot-protection            Enables cold boot attack protection."
                                 echo "    --self-destruct                   Makes script self-destruct."
                                 echo "    --safest-level                    Starts Tor Browser with the safest security level by default."
                                 echo "    --save-bash-history               Prevents bash history from being cleared."
                                 echo "    --x11-sandbox                     Runs the Tor Browser in an X11 sandbox with Xephyr."
                                 echo "    -p, --preserve                    Prevents the Tor Browser sandbox from being deleted or any directories being unmounted at the end of the script."
                                 echo "    -s, --start-tor-browser           Starts Tor Browser after the sandbox has been created with the '-p' flag. DO NOT USE THIS BEFORE CREATING THE SANDBOX."
                                 echo "    --firewall                        Creates firewall that only allows Tor traffic. Applies to host and sandbox."
                                 echo "    -d, --debug                       Starts chroot as root without Tor Browser for debugging purposes."
                                 echo "    -du, --debug-user                 Starts chroot as user without Tor Browser for debugging purposes."
                                 echo "    --cleanup                         Unmounts everything inside sandbox, deletes it and deletes the AppArmor profiles. Meant for after use of the '-p' flag."
                                 echo "    -m, --multiple-options            Allows user to use multiple flags."
                                 echo "    -h, --help                        Show this message."
                                 echo ""
                                 echo "Don't use any of these unless you know exactly what you're doing. They may break things or weaken security. Multiple flags cannot be used together unless with the multiple options flag.
                                 "
                                 exit 1
                                 ;;
                *)
                                 echo "'$*' is not a command. Please try again."
                                 echo "Run './torbrowser-sandbox.sh --help' for more information."
                                 exit 1
                                 ;;
        esac
done

script_checks() {
  ## Checks if sudo works.
  if ! sudo id &>/dev/null; then
    printf "\\n${red}%s${endc}\\n" \
           "WARNING: SUDO IS NOT CONFIGURED PROPERLY. THIS SCRIPT CANNOT RUN. EXITING."
    exit 1
  fi

  ## Checks if /tmp/torbrowser-sandbox already exists.
  if [ -d /tmp/torbrowser-sandbox/ ]; then
    printf "\\n${red}%s${endc}\\n" \
           "WARNING: THE SANDBOX HAS AlREADY BEEN CREATED. MAYBE YOU WANT THE '-s' FLAG? EXITING."
    exit 1
  fi

  ## Checks if /tmp is mounted as a tmpfs.
  if ! mount | grep "tmpfs on /tmp type tmpfs" >/dev/null; then
    printf "\\n${red}%s${endc}\\n" \
           "WARNING: /tmp IS NOT MOUNTED AS A TMPFS. EXITING."
    exit 1
  fi

  ## Exit if not on Arch Linux.
  if ! grep -q "Arch Linux" /etc/os-release; then
    printf "\\n${red}%s${endc}\\n" \
           "WARNING: THIS SCRIPT IS ONLY MEANT TO BE USED ON ARCH LINUX. EXITING."
    exit 1
  fi

}

enable_apparmor() {
  ## Enable AppArmor
  if ! [ "${disable_apparmor}" = "1" ]; then

    # Checks if AppArmor is enabled on the host, if not, exits.
    if ! sudo aa-status --enabled; then
      printf "\\n${red}%s${endc}\\n" \
             "WARNING: APPARMOR IS NOT ENABLED. ENABLE IT OR RUN THIS SCRIPT WITH THE '--disable-apparmor' FLAG. EXITING."
      exit 1
    fi

    # Checks if AppArmor profiles are already there.
    if [ -f /etc/apparmor.d/torbrowser-sandbox.firefox.real ]; then
      printf "\\n${red}%s${endc}\\n" \
             "WARNING: THE APPARMOR PROFILES ARE ON THE HOST ALREADY. REMOVE THEM. EXITING."
      exit 1
    fi

    # Installs git if not already.
    if ! pacman -Qq git &>/dev/null; then
      sudo pacman -S --noconfirm -q git >/dev/null
      remove_git=1
    fi

    printf "\\n${green}%s${endc}\\n" \
           "Enabling AppArmor...
    "

    # Clones gitlab repostory so we can use the AppArmor profiles.
    git clone https://gitlab.com/madaidan/sandboxed-tor-browser.git /tmp/sandboxed-tor-browser/

    # Copies AppArmor profiles to /etc/apparmor.d.
    sudo cp /tmp/sandboxed-tor-browser/apparmor.d/tunables/* /etc/apparmor.d/tunables/
    sudo cp /tmp/sandboxed-tor-browser/apparmor.d/abstractions/* /etc/apparmor.d/abstractions/
    sudo cp /tmp/sandboxed-tor-browser/apparmor.d/torbrowser-sandbox.* /etc/apparmor.d/

    # Enforces AppArmor profiles.
    sudo aa-enforce /etc/apparmor.d/torbrowser-sandbox.Tor.tor
    sudo aa-enforce /etc/apparmor.d/torbrowser-sandbox.firefox.real

    # Copies Xephyr and Openbox AppArmor profile to /etc/apparmor.d and enforces them.
    if [ "${x11_sandbox}" = "1" ]; then
      sudo cp /tmp/sandboxed-tor-browser/apparmor.d/usr.bin.Xephyr-sandbox /etc/apparmor.d/
      sudo cp /tmp/sandboxed-tor-browser/apparmor.d/torbrowser-x11-sandbox-openbox /etc/apparmor.d/
      sudo aa-enforce /etc/apparmor.d/usr.bin.Xephyr-sandbox
      sudo aa-enforce /etc/apparmor.d/torbrowser-x11-sandbox-openbox
    fi

    # Checks if AppArmor profiles are enabled. If not, exits the script.
    if ! sudo aa-status | grep "/tmp/torbrowser-sandbox/home/user/tor-browser_en-US/Browser/firefox.real" >/dev/null; then
      printf "\\n${red}%s${endc}\\n" \
             "ERROR: AppArmor has not been enabled. Is AppArmor enabled on your host? Exiting.
      "
      exit 1
    else
      printf "\\n${green}%s${endc}\\n" \
             "AppArmor is enabled.
      "
    fi
  fi
}

disable_swap() {
  ## Disables swap. Swap can write parts of RAM to disk. Swap will be enabled again on the next boot and at the end of the script.
  if ! [ "${enable_swap}" = "1" ]; then
    # Gets vm.swappiness value and saves it as a variable. Used for re-enabling swap.
    swappiness_value=$(sysctl vm.swappiness | awk '{print $3}')

    # Makes the kernel only swap if absolutely necessary.
    sudo sysctl vm.swappiness=1 >/dev/null

    # Checks if swap is enabled. If so, is re-enabled later in the script. This is to prevent enabling an unwanted swap partition.
    if free | awk '/^Swap:/ {exit !$2}'; then
      enable_swap_partitions=1
    fi

    # Disables swap completely.
    #sudo swapoff -a
  else
    # Tells '-s' flag not to enable swap.
    if [ "${preserve}" = "1" ]; then
      echo "1" | sudo tee /tmp/torbrowser-sandbox/root/enable_swap >/dev/null
    fi
  fi
}

kill_tor_browser() {
  ## Kill Tor Browser if started on host.
  printf "\\n${green}%s${endc}\\n" \
         "Checking if Tor Browser is running...
  "

  # Checks if Tor Browser is running. firefox.real is the browser part.
  if sudo ps aux | grep "[f]irefox.real"; then
    printf "\\n${red}%s${endc}\\n" \
           "Tor Browser is running on the host. Killing it...
    "

    # Kills Tor Browser.
    sudo kill $(sudo ps aux | grep "[f]irefox.real" | awk -F':' '{print $1}' | awk '{print $(2)}') &>/dev/null
    sudo kill $(sudo ps aux | grep "[T]orBrowser/Tor/tor" | awk -F':' '{print $1}' | awk '{print $(2)}') &>/dev/null

    printf "\\n${green}%s${endc}\\n" \
           "Successfully killed Tor Browser.
    "
  else
    printf "\\n${green}%s${endc}\\n" \
           "Tor Browser is not running.
    "
  fi
}

create_chroot_jail() {
  ## Create chroot jail.
  printf "\\n${green}%s${endc}\\n" \
         "Creating chroot jail...
  "

  # Creates initial directory.
  mkdir -m 755 /tmp/torbrowser-sandbox/

  # Install 'filesystem' package to create necessary folders. var/lib/pacman is needed for pacman to run.
  mkdir -p /tmp/torbrowser-sandbox/var/lib/pacman/
  chmod -R 0755 /tmp/torbrowser-sandbox/var/

  # Uses '-Sy' because database isn't synced in chroot yet.
  sudo pacman -Sy --noconfirm -q filesystem -r /tmp/torbrowser-sandbox/ >/dev/null

  # Create user's home directory.
  sudo mkdir -m 755 /tmp/torbrowser-sandbox/home/user/
  sudo mkdir -m 755 /tmp/torbrowser-sandbox/home/user/Downloads

  printf "\\n${green}%s${endc}\\n" \
         "Successfully created chroot jail.
  "
}

install_tor_browser() {
  # Checks if Tor Browser is in your home directory and if it is, copies it to chroot jail.
  if [ -d "${HOME}/Downloads/torbrowser-sandbox" ]; then
    sudo cp -a "${HOME}/Downloads/torbrowser-sandbox" "/tmp/torbrowser-sandbox/home/user/tor-browser_en-US/" # Original directory name may not end in '_en-US' but this isn't a problem.
  else
    # Downloads Tor Browser and verifies it if it isn't in your home directory.
    printf "\\n${green}%s${endc}\\n" \
           "Installing Tor Browser...
           "

    # Downloads TBB.
    sudo ${scurl} "https://www.torproject.org/dist/torbrowser/${tbb_ver}/tor-browser-linux64-${tbb_ver}_en-US.tar.xz" -o "/tmp/torbrowser-sandbox/home/user/tor-browser-linux64-${tbb_ver}_en-US.tar.xz"

    # Downloads signature.
    sudo ${scurl} "https://dist.torproject.org/torbrowser/${tbb_ver}/tor-browser-linux64-${tbb_ver}_en-US.tar.xz.asc" -o "/tmp/torbrowser-sandbox/home/user/tor-browser-linux64-${tbb_ver}_en-US.tar.xz.asc"

    # Fixes permissions.
    sudo chmod 755 /tmp/torbrowser-sandbox/home/user/tor-browser-linux64-"${tbb_ver}"_en-US.tar.xz*

    # Import GPG key.
    gpg --keyserver pool.sks-keyservers.net --recv-keys 0x4E2C6E8793298290 &>/dev/null

    # Verify TBB.
    if ! gpg --status-fd 1 --verify "/tmp/torbrowser-sandbox/home/user/tor-browser-linux64-${tbb_ver}_en-US.tar.xz.asc" "/tmp/torbrowser-sandbox/home/user/tor-browser-linux64-"${tbb_ver}"_en-US.tar.xz" &>/dev/null; then
      printf "\\n${red}%s${endc}\\n" \
             "WARNING: The Tor Browser could not be verified. This may be the result of a compromised download. Exiting."
      # Remove directories. and AppArmor profiles.
      sudo rm -rf /tmp/sandboxed-tor-browser/
      sudo rm -rf /tmp/torbrowser-sandbox/

      # Remove AppArmor definitions.
      sudo apparmor_parser -R /etc/apparmor.d/torbrowser-sandbox.Tor.tor /etc/apparmor.d/torbrowser-sandbox.firefox.real

      # Shred AppArmor profiles.
      sudo shred -zu ${apparmor_profiles}

      # Delete AppArmor profiles.
      sudo rm ${apparmor_profiles}

      # Remove Xephyr AppArmor profile.
      if [ -f /etc/apparmor.d/usr.bin.Xephyr-sandbox ]; then
        sudo apparmor_parser -R /etc/apparmor.d/usr.bin.Xephyr-sandbox
        sudo shred -zu /etc/apparmor.d/usr.bin.Xephyr-sandbox

        # Remove Openbox AppArmor profile.
        sudo apparmor_parser -R /etc/apparmor.d/torbrowser-x11-sandbox-openbox &>/dev/null # The output is sent to /dev/null because it gets an error about tunables/global-sandbox not existing otherwise.
        sudo shred -zu /etc/apparmor.d/torbrowser-x11-sandbox-openbox
      fi
      exit 1
    else
      # Extract Tor Browser and fix permissions.
      sudo tar -xJf /tmp/torbrowser-sandbox/home/user/tor-browser-linux64-"${tbb_ver}"_en-US.tar.xz -C /tmp/torbrowser-sandbox/home/user/
      sudo chmod -R 700 /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/
      sudo chown 1000 -R /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/

      # Checks if it was installed correctly.
      if [ -d /tmp/torbrowser-sandbox/home/user/tor-browser_en-US ]; then
        printf "\\n${green}%s${endc}\\n" \
               "Tor Browser successfully installed.
               "
      else
        printf "\\n${red}%s${endc}\\n" \
               "ERROR: THE TOR BROWSER WAS NOT INSTALLED PROPERLY.
               "
      fi

      # Cleanup.
      sudo shred -zu /tmp/torbrowser-sandbox/home/user/tor-browser-linux64-"${tbb_ver}"_en-US.tar.xz.asc /tmp/torbrowser-sandbox/home/user/tor-browser-linux64-"${tbb_ver}"_en-US.tar.xz
      gpg --delete-key 0x4E2C6E8793298290
    fi
  fi
}

prepare_chroot() {
  printf "\\n${green}%s${endc}\\n" \
         "Configuring chroot jail...
  "

  # Sets default DNS server to 0.0.0.0. Prevents some clearnet access and may prevent some clearnet leaks. An attacker can just use an IP instead of a hostname to get around this.
  echo "nameserver 0.0.0.0" | sudo tee /tmp/torbrowser-sandbox/etc/resolv.conf >/dev/null

  # Prevents root from changing /etc/resolv.conf by changing the permissions and setting the immutable attribute. Although root can just change the permissions back.
  sudo chmod 444 /tmp/torbrowser-sandbox/etc/resolv.conf

  # SSL certificates.
  sudo cp -r /etc/ca-certificates /tmp/torbrowser-sandbox/etc/ca-certificates/
  sudo chmod 755 -R /tmp/torbrowser-sandbox/etc/ca-certificates/

  # Install xhost if not already. Allows other users (in this case the user in the chroot jail) to use the same X server. Will be removed if not already installed at the end of the script.
  if ! [ "${x11_sandbox}" = "1" ]; then
    if ! pacman -Qq xorg-xhost &>/dev/null; then
      sudo pacman -S --noconfirm -q xorg-xhost >/dev/null
      remove_xhost=1
    fi

    # Allow chroot to use the same X server. Also allows other users on the system to use the same X server. May not be wanted on a multi-user system.
    xhost +local: >/dev/null
  fi

  # Install needed packages in chroot - needed to run Tor Browser.
  sudo pacman -S --noconfirm -q libcanberra mailcap gdk-pixbuf2 gtk3 dbus-glib libxt openssl-1.0 libevent -r /tmp/torbrowser-sandbox &>/dev/null

   # Installs bubblewrap. Needed for sandboxing.
  if ! pacman -Qq bubblewrap &>/dev/null; then
    sudo pacman -S --noconfirm -q bubblewrap >/dev/null
    remove_bubblewrap=1
  fi

  ## Anonymizes machine-ids - uses the same machine-id as Whonix and Tails.
  echo "b08dfa6083e7567a1921a715000001fb" | sudo tee /tmp/torbrowser-sandbox/var/lib/dbus/machine-id >/dev/null
  echo "b08dfa6083e7567a1921a715000001fb" | sudo tee /tmp/torbrowser-sandbox/etc/machine-id >/dev/null

  # Sets safest security level on Tor Browser.
  if [ "${safest_level}" = "1" ]; then
    echo '''user_pref("extensions.torbutton.security_slider", 1);''' | sudo tee -a /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/Browser/TorBrowser/Data/Browser/profile.default/prefs.js >/dev/null

    # Sets permissions of prefs.js.
    sudo chown 1000 /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/Browser/TorBrowser/Data/Browser/profile.default/prefs.js
    sudo chmod 600 /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/Browser/TorBrowser/Data/Browser/profile.default/prefs.js
  fi

  # Puts display variable in /tmp/torbrowser-sandbox/display.
  echo "${DISPLAY}" | cut -c1-2 | sudo tee /tmp/torbrowser-sandbox/display >/dev/null
  sudo chmod 444 /tmp/torbrowser-sandbox/display

  # Script to run in chroot.
  cat <<EOF > /tmp/torbrowser-sandbox/script.sh
#!/bin/bash

# Starts Tor Browser and uses host's X server.
DISPLAY=\$(cat /display) /home/user/tor-browser_en-US/Browser/firefox.real &>/dev/null
EOF

  # Sets permissions of script.
  sudo chmod 555 /tmp/torbrowser-sandbox/script.sh

  # Remove su binary.
  sudo shred -zu /tmp/torbrowser-sandbox/bin/su

  # Create dummy su binary.
  echo "#!/bin/bash -e

exit 0" | sudo tee /tmp/torbrowser-sandbox/bin/su >/dev/null

  # Prevent certain binaries from being executed or files being read in the chroot.
  sudo chmod 000 /tmp/torbrowser-sandbox/bin/su # Useful for preventing root privileges.
  sudo chmod 000 /tmp/torbrowser-sandbox/bin/rm # Useful for preventing deleting files.
  sudo chmod 000 /tmp/torbrowser-sandbox/bin/chmod # Prevents an attacker from changing back permissions. Can still be bypassed.
  sudo chmod 000 /tmp/torbrowser-sandbox/bin/chown # Prevents an attacker from changing the owner of a file.
  sudo chmod 000 /tmp/torbrowser-sandbox/bin/chroot # Prevents an attacker from running chroot which can be used to break out of chroot jail. Can still be bypassed.

  # Remove all setuid and setgid bits from binaries.
  sudo chmod -R u-s /tmp/torbrowser-sandbox/
  sudo chmod -R g-s /tmp/torbrowser-sandbox/

  # Lock the root account.
  sudo passwd -l root -R /tmp/torbrowser-sandbox/

  printf "\\n${green}%s${endc}\\n" \
         "Successfully configured chroot jail.
  "
}

create_x11_sandbox() {
  # Create X11 sandbox.
  if [ "${x11_sandbox}" = "1" ]; then
    printf "\\n${green}%s${endc}\\n" \
          "Creating X11 sandbox...
     "

    # Installs Xephyr.
    if ! pacman -Qq xorg-server-xephyr &>/dev/null; then
      sudo pacman -S --noconfirm -q xorg-server-xephyr >/dev/null
      remove_xephyr=1
    fi

    # Installs xrandr.
    if ! pacman -Qq xorg-xrandr &>/dev/null; then
      sudo pacman -S --noconfirm -q xorg-xrandr >/dev/null
      remove_xrandr=1
    fi

    # Installs openbox.
    sudo pacman -S --noconfirm -q openbox -r /tmp/torbrowser-sandbox >/dev/null


    # Custom openbox menu configuration to avoid confusion.
    echo '''
<?xml version="1.0" encoding="UTF-8"?>

<openbox_menu xmlns="http://openbox.org/3.4/menu">

<menu id="apps-net-menu" label="Internet">
  <item label="Tor Browser">
    <action name="Execute">
      <command>/script.sh</command>
      <startupnotify>
        <enabled>yes</enabled>
        <wmclass>Firefox</wmclass>
      </startupnotify>
    </action>
  </item>
</menu>

<menu id="root-menu" label="Openbox 3">
  <separator label="Applications" />
  <menu id="apps-net-menu"/>
</menu>

</openbox_menu>''' | sudo tee /tmp/torbrowser-sandbox/etc/xdg/openbox/menu.xml >/dev/null

    # Remove multiple openbox workstations to avoid confusion by accidentally switching between them.
    sudo sed -i 's/<number>4<\/number>/<number>1<\/number>/' /tmp/torbrowser-sandbox/etc/xdg/openbox/rc.xml

    # Starts Xephyr in sandbox.
    ${bwrap_x11_sandbox} /usr/bin/Xephyr -ac "${xephyr_display}" &>/dev/null &

    # Starts openbox in sandbox.
    sudo sed -i '3i\DISPLAY=$(cat /display) openbox &>/dev/null &\' /tmp/torbrowser-sandbox/script.sh

    # Adds new display variable to script.sh.
    echo "${xephyr_display}" | sudo tee /tmp/torbrowser-sandbox/display >/dev/null

    # Changes resolution of Xephyr window to 1024x768.
    sleep 1
    xrandr --display "${xephyr_display}" --output default --mode 1024x768 &>/dev/null

    # Used for telling the "-s" flag to start Xephyr.
    if [ "${preserve}" = "1" ]; then
      echo "1" | sudo tee /tmp/torbrowser-sandbox/root/x11 >/dev/null
    fi

    printf "\\n${green}%s${endc}\\n" \
          "Successfully created X11 sandbox.
     "
  fi
}

start_firewall() {
  ## Create firewall to prevent clearnet leaks.
  if [ "${firewall}" = "1" ]; then
    printf "\\n${green}%s${endc}\\n" \
           "Creating firewall to prevent clearnet leaks...
    "

    # Creates firewall.
    create_firewall

    # Checks if firewall is working.
    if sudo iptables-save | grep "-A OUTPUT -m owner --uid-owner tor -j ACCEPT" >/dev/null; then
      printf "\\n${green}%s${endc}\\n" \
             "Successfully created firewall.
     "
    else
      printf "\\n${red}%s${endc}\\n" \
             "The firewall has not been created. This may be a bug.
      "
    fi

    # Used for telling the "-s" flag to start the firewall.
    if [ "${preserve}" = "1" ]; then
      echo "1" | sudo tee /tmp/torbrowser-sandbox/root/firewall >/dev/null
    fi
  fi
}

stronger_dac() {
  ## Stronger DAC/filesystem permissions.

  # Allow only reading and executing of binaries.
  sudo chmod 555 -R /tmp/torbrowser-sandbox/bin/

  # Allow only reading and executing of libraries.
  sudo chmod 555 -R /tmp/torbrowser-sandbox/lib/

  # Allow only reading and executing of configuration files.
  sudo chmod 555 -R /tmp/torbrowser-sandbox/etc/

  # Allow only user to access their home directory.
  sudo chmod 700 /tmp/torbrowser-sandbox/home/user/
  sudo chown 1000 /tmp/torbrowser-sandbox/home/user/

}

start_tor_browser() {
  printf "\\n${green}%s${endc}\\n" \
        "Starting Tor Browser...
   "

  # Creates user.
  echo "user:x:1000:1000::/home/user:/bin/bash" | sudo tee -a /tmp/torbrowser-sandbox/etc/passwd >/dev/null

  # Makes sure Tor Browser is killed.
  kill_tor_browser

  # Chroot into sandbox and start Tor Browser.
  sudo ${bwrap_sandbox} chroot --userspec=user /tmp/torbrowser-sandbox/ /script.sh

  # Makes sure Tor Browser is killed.
  kill_tor_browser
}

cleanup() {
  ## Prevents other users using X server.
  if ! [ "${preserve}" = "1" ]; then
    if ! [ "${x11_sandbox}" = "1" ]; then
      xhost - >/dev/null
    fi
  fi

  ## Clean up packages installed on main system.
  if ! [ "${preserve}" = "1" ]; then

    # Uninstalls xhost.
    if [ "${remove_xhost}" = "1" ]; then
      sudo pacman -Rn --noconfirm xorg-xhost >/dev/null
    fi

    # Uninstalls git.
    if [ "${remove_git}" = "1" ]; then
      sudo pacman -Rn --noconfirm git >/dev/null
    fi

    # Uninstalls bubblewrap.
    if [ "${remove_bubblewrap}" = "1" ]; then
      sudo pacman -Rn --noconfirm bubblewrap >/dev/null
    fi

    # Uninstalls xephyr.
    if [ "${remove_xephyr}" = "1" ]; then
      sudo pacman -Rn --noconfirm xorg-server-xephyr >/dev/null
    fi

    # Uninstalls xrandr.
    if [ "${remove_xrandr}" = "1" ]; then
      sudo pacman -Rn --noconfirm xorg-xrandr >/dev/null
    fi

    # Uninstalls make.
    if [ "${remove_make}" = "1" ]; then
      sudo pacman -Rn --noconfirm make >/dev/null
    fi

    # Uninstalls ipset.
    if [ "${remove_ipset}" = "1" ]; then
      sudo pacman -Rn --noconfirm ipset >/dev/null
    fi
  fi
}

remove_apparmor_profiles() {
  ## Remove AppArmor profiles.
  if ! [ "${preserve}" = "1" ]; then
    if ! [ "${disable_apparmor}" = "1" ]; then
      printf "\\n${green}%s${endc}\\n" \
            "Removing Tor Browser AppArmor profiles...
  "

      # Remove AppArmor definitions.
      sudo apparmor_parser -R /etc/apparmor.d/torbrowser-sandbox.Tor.tor /etc/apparmor.d/torbrowser-sandbox.firefox.real

      # Shred AppArmor profiles.
      sudo shred -zu ${apparmor_profiles}

      # Remove Xephyr and Openbox AppArmor profiles.
      if [ "${x11_sandbox}" = "1" ]; then
        sudo apparmor_parser -R /etc/apparmor.d/usr.bin.Xephyr-sandbox
        sudo shred -zu /etc/apparmor.d/usr.bin.Xephyr-sandbox

        sudo apparmor_parser -R /etc/apparmor.d/torbrowser-x11-sandbox-openbox &>/dev/null # The output is sent to /dev/null because it gets an error about tunables/global-sandbox not existing otherwise.
        sudo shred -zu /etc/apparmor.d/torbrowser-x11-sandbox-openbox
      fi

      if ! [ -f /etc/apparmor.d/torbrowser-sandbox.firefox.real ]; then
        printf "\\n${green}%s${endc}\\n" \
               "Tor Browser AppArmor profiles have been removed.
       "
      else
        printf "\\n${red}%s${endc}\\n" \
               "ERROR: Tor Browser AppArmor profiles have not been removed. This is most likely a bug.
       "
      fi
    fi
  fi
}

delete_sandbox() {
  ## Unmount filesystems in chroot jail and delete /tmp/torbrowser-sandbox.
  if ! [ "${preserve}" = "1" ]; then
    printf "\\n${green}%s${endc}\\n" \
          "Unmounting filesystems and deleting chroot jail...
    "
    if ! [ "${cold_boot_protection}" = "1" ]; then
      # Deletes chroot jail.
      sudo rm -rf /tmp/torbrowser-sandbox

      # Deletes AppArmor profile download folder.
      if [ -d /tmp/sandboxed-tor-browser/ ]; then
        sudo rm -rf /tmp/sandboxed-tor-browser/
      fi

      # Checks if chroot jail has been deleted.
      if ! [ -d /tmp/torbrowser-sandbox ]; then
        printf "\\n${green}%s${endc}\\n" \
               "Chroot jail deleted succesfully.
        "
      else
        printf "\\n${red}%s${endc}\\n" \
               "The chroot jail has not been deleted. This may be a bug.
               "
      fi
    fi
  fi
}

cold_boot_attack_protection() {
  ## Shred Tor Browser. Prevents cold-boot attacks. Too slow and ineffective. Needs good alternative (maybe wipe?).
  if [ "${cold_boot_protection}" = "1" ]; then
    # Shreds and deletes chroot jail.
    if [ -d /tmp/torbrowser-sandbox/ ]; then
      printf "\\n${green}%s${endc}\\n" \
            "Shredding chroot jail (cold boot attack protection)...
       "

      # Shred files.
      sudo find /tmp/torbrowser-sandbox/ -type f -exec shred -zu {} \;

      # Overwrite file names.
      #for i in $(sudo find /tmp/torbrowser-sandbox);
      #do
      #  sudo mv "$i" /tmp/torbrowser-sandbox/"${RANDOM}${RANDOM}${RANDOM}";
      #done

      # Deletes files.
      sudo rm -rf /tmp/torbrowser-sandbox/

      if ! [ -d /tmp/torbrowser-sandbox ]; then
        printf "\\n${green}%s${endc}\\n" \
               "Chroot jail shredded succesfully.
        "
      else
        printf "\\n${red}%s${endc}\\n" \
               "The chroot jail has not been shredded. You are vulnerable to cold boot attacks. This may be a bug.
               "
      fi
    else
      printf "\\n${red}%s${endc}\\n" \
             "The chroot jail has not been shredded as it is not there. You are vulnerable to cold boot attacks. This may be a bug.
             "
    fi

    # Shreds /tmp/sandboxed-tor-browser which is where the AppArmor profiles are saved to with 'git clone'.
    if [ -d /tmp/sandboxed-tor-browser ]; then
      printf "\\n${green}%s${endc}\\n" \
             "Shredding AppArmor profile download folder (cold boot attack protection)...
      "

      # Shreds files
      sudo find /tmp/sandboxed-tor-browser/ -type f -exec shred -zu {} \;

      # Overwrite file names.
      #for i in $(sudo find /tmp/sandboxed-tor-browser);
      #do
      #  sudo mv "$i" /tmp/sandboxed-tor-browser/"${RANDOM}${RANDOM}${RANDOM}";
      #done

      # Delete files.
      sudo rm -rf /tmp/sandboxed-tor-browser/

      if ! [ -d /tmp/sandboxed-tor-browser/ ]; then
        printf "\\n${green}%s${endc}\\n" \
               "AppArmor profile download folder shredded succesfully.
    "
      else
        printf "\\n${red}%s${endc}\\n" \
               "AppArmor profile download folder has not been shredded. You are vulnerable to cold boot attacks. This may be a bug.
    "
      fi
    fi
  fi
}

reenable_swap() {
  ## Re-enable swapping.
  if ! [ "${enable_swap}" = "1" ]; then
    # Sets vm.swappiness value back to default.
    sudo sysctl vm.swappiness="${swappiness_value}" >/dev/null

    # Enables swap partitions/files again.
    #if [ "${enable_swap_partitions}" = "1" ]; then
    #  sudo swapon -a
    #fi
  fi
}

clear_bash_history() {
  ## Clear bash history.
  if ! [ "${save_bash_history}" = "1" ]; then
    # Clears all users' history except the root user.
    echo "" | sudo tee "${HOME}"/.bash_history

    # Clears root user's bash history.
    echo "" | sudo tee /root/.bash_history

    # Clears history.
    history -c
  fi
}

restore_iptables() {
  ## Restore iptables rules.
  if [ "${firewall}" = "1" ]; then
    sudo iptables-restore < /tmp/iptables-rules
    if [ -f /proc/net/if_inet6 ]; then
      sudo ip6tables-restore < /tmp/ip6tables-rules
    fi

    # Delete /tmp/iptables-rules.
    sudo shred -zu /tmp/iptables-rules

    # Re-enable UFW if needed.
    if [ "${reenable_ufw}" = "1" ]; then
      sudo ufw enable >/dev/null
    fi
  fi
}

self_destruct() {
  ## Self destruct. Leave no traces of script.
  if [ "${self_destruct}" = "1" ]; then
    shred -zu "$(realpath "$0")"
  fi
}

script_checks
enable_apparmor
disable_swap
kill_tor_browser
create_chroot_jail
install_tor_browser
prepare_chroot
create_x11_sandbox
start_firewall
stronger_dac
start_tor_browser
cleanup
remove_apparmor_profiles
delete_sandbox
cold_boot_attack_protection
reenable_swap
clear_bash_history
restore_iptables
self_destruct
