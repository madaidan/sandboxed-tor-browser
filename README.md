# Forensics Resistant Tor Browser Sandbox

WARNING: This project is currently dead and contains tons of outdated/wrong/stupid stuff. Use at your own risk.

This can only be used on Arch Linux but should be easy to port to other distros.

TL;DR: This creates a secure environment where you can run the Tor Browser where all data is lost at shutdown and cannot be recovered (assuming a cold boot attack doesn't happen).

This is a bash script that creates a chroot jail in /tmp sandboxed with [bubblewrap](https://github.com/projectatomic/bubblewrap), places the Tor Browser in it and restricts it with AppArmor.

Host -> Bubblewrap container -> Chroot jail -> AppArmor -> Tor Browser

The purpose of this is to create a forensics resistant Tor Browser sandbox that is really secure.

/tmp is a tmpfs which means all data in it is stored in RAM. This means none or very little data is written to disk making it resistant to local forensics.

The Tor Browser is not totally resistant to forensics by itself. See https://research.torproject.org/techreports/tbb-forensic-analysis-2013-06-28.pdf for more information on this. It may also leave some things like caching behind.

The AppArmor profiles for the Tor Browser are modified versions of [Micah Lee's profiles](https://github.com/micahflee/torbrowser-launcher/tree/develop/apparmor). Credit to him for making them.

This script will kill any Tor Browser opened on your system at the start of the script. This is because it causes errors when two Tor Browser's are opened at the same time. Make sure you aren't doing anything on the Tor Browser or close it manually before running the script.

This does not make any changes to the Tor Browser package.

This script also disables swap by setting `vm.swappiness=1` in sysctl. This makes the kernel only swap when absolutely necessary. This doesn't make swapping impossible and in the future the script might run `swapoff -a` to make sure swap is completely disabled. `vm.swappiness` gets reset to it's original value at the end of the script. This prevents data from being written to disk from RAM through swapping.

Make sure you have at least 1.2GiB of spare RAM before running this script.

This script doesn't need to be run with `sudo` or as the root user as the script uses `sudo` for commands that need it.

This script does not enable AppArmor on your host and does not secure your host. If you don't have AppArmor enabled on your host then you will need to run this script with the `--disable-apparmor` flag. See below for more details on the flags.

This script clears your bash history to prevent someone from knowing you have run this.

This script may take long to finish, especially in the chroot jail creation part. It has always completed within a short time period for me though.

If you are finished with the Tor Browser then DO NOT quit the script by pressing Ctrl + C while in the terminal, closing the terminal tab etc. Just close the Tor Browser by tapping the X at the top right corner. The script has lots of cleaning up to do after the Tor Browser has closed. Closing it in other methods will terminate the script and prevent other important parts from running.

## Limitations

1. It isn't totally forensics resistant. It is still vulnerable to [cold-boot attacks](https://en.wikipedia.org/wiki/Cold_boot_attack) where an attacker can analyse the data while it is still in RAM. RAM usually stays for a few minutes after your computer being shut down. If an attacker cools the RAM sticks e.g. by sticking it in a fridge then data can stay for up to a few hours, making it much more easier to make a copy of the data in RAM.

- It may be possible to prevent this by shredding the Tor Browser while it is still in /tmp before shutting down. This will overwrite all of its data with zeroes, making it impossible or very hard to perform cold boot attacks. There is a mechanism in the script for this but it is disabled by default as it will be very slow and ineffective. If I find a more efficent way then this will be enabled by default.

2. Data in RAM can be written to disk through swapping/paging. `vm.swappiness=1` (which the script uses) does not disable swap completely but only partially disables it.

3. It relies on your host operating system. If your host is compromised then this sandbox will also be compromised. There is nothing that can be done about this except you hardening your host and making it secure.

## Tails

If you want true amnesia and forensics resistance then I urge you to use [Tails](https://tails.boum.org/). It is a live operating system, designed to be run from a USB stick or CD. All data is stayed in RAM and nothing is written to disk. It solves cold boot attacks by erasing all data in RAM before shutdown. All network connections are safely forced through Tor.

## How to use it

To use it, download torbrowser-sandbox.sh from this repository, run `chmod +x torbrowser-sandbox.sh` and run `./torbrowser-sandbox.sh`.

This script downloads a fresh version of the Tor Browser from https://torproject.org and verifies it automatically with GPG. If the the download cannot be verified then then the script will end as it is likely a malicious download.

If you do not want the script to download a new Tor Browser every time, you can download the Tor Browser yourself, extract and save it in Downloads/torbrowser-sandbox in your home directory and the script will copy it to the sandbox every time it is run without having to download another Tor Browser. The browser that you've downloaded yourself should not have been used before as this will mess with some configurations.

Some things may not work in the sandbox. Things such as audio, webcams, microphones etc. will not work as they'd just be extra attack surface.

If you are going to be using this script a lot, you can place it in /bin and it can be started with just `torbrowser-sandbox`. Make sure you have set the correct permissions for the script.

Flags cannot be run together due to a limitation in bash or the script (I'm not sure if it's possible in bash). I've created a workaround where you need to run the script with the `--multiple-options` or `-m` flag and then enter your other flags. Some flags cannot be run together such as `--cold-boot-protection` or `--preserve` as these would break the script. You cannot use the short versions of the flags when using multiple flags. You will need to add the full flag.

Run it with the flag `--disable-apparmor` to disable AppArmor. You might want this if AppArmor isn't enabled on your system. If AppArmor is enabled then you should not use this flag unless you know what you're doing. AppArmor is a very important security feature.

To keep swap enabled, run it with the flag `--enable-swap`. This may allow some data to be saved to disk through swapping.

To enable cold boot attack protection, run it with the flag `--cold-boot-protection`. This shreds the entire sandbox with `shred -z`. This overwrites all of the data in the sandbox with random data and then with a bunch of zeroes. I have also implemented a part where it overwrites all of the file names as well by renaming every file/directory to random names. This takes a long time.

To make the script self-destruct at the end of the script so someone won't know you've used this script, run it with the flag `--self-destruct` (untested and will probably break). This will make the script shred and delete itself.

To start the Tor Browser with the safest security level by default, run it with the flag `--safest-level`. This only changes the default settings on the security slider. This does not make anymore changes to the settings than an ordinary Tor Browser with the security slider at the top. This isn't enabled by default for the same reasons the ordinary Tor Browser doesn't enable it by default.

To prevent the bash history from being cleared, run it with the flag `--save-bash-history`.

To prevent the Tor Browser sandbox from being deleted at the end of the script or any directories being unmounted, run it with the flag `--preserve` or `-p`. Everything will still be cleared at shutdown due to the tmpfs. This allows you to close and re-open the sandboxed Tor Browser multiple times without it deleting. To open the Tor Browser a second time after the chroot jail has been made with the `--preserve` or `-p` flag, run the script with the `--start-tor-browser` or `-s` flag. This does not persist between reboots. This will also save the AppArmor profiles which will persist each reboot. Run the script with the `--cleanup` flag to remove them. Packages installed during the script are also not removed.

To chroot into the sandbox as root, without starting the Tor Browser, run it with the flag `--debug` or `-d`. This should only ever be used for debugging purposes.

To chroot into the sandbox as a user, without starting the Tor Browser, run it with the flag `--debug-user` or `-du`. This should only ever be used for debugging purposes.

To use an X11 sandbox with Xephyr, run it with the flag `--x11-sandbox`. This starts Xephyr (an external X server) in a bubblewrap sandbox on your host and makes the Tor Browser use that instead of your main X server. Xorg provides no isolation between windows so any GUI application can access another and do things like use keyloggers or screenshot programs. An X11 sandbox prevents the Tor Browser from doing those things and increases security. It uses Openbox as a window manager so you can access drop down menus like the settings bar. Openbox is started within the same sandbox the Tor Browser is in. Xephyr and Openbox are also restricted with AppArmor.

To enable the firewall, run it with the flag `--firewall`. This configures iptables to only allow outgoing Tor Browser traffic. Everything else is blocked. This applies for the host and sandbox so your host's networking will also be firewalled. This prevents clearnet leaks so it is harder for an attacker to leak your IP address. This saves all current iptables rules and disables UFW. Iptables rules and UFW are both restored at the end of the script. (Broken)

If you have run the script with the `--preserve` flag and wish to delete the sandbox and AppArmor profiles then run the script with the `--cleanup` flag.

Run it with the flag `--help` to get a list of options.

## Known Issues

There are a few issues with this script. They will be fixed once I figure out how to fix them.

1. Clearnet leaks are possible by escaping the Tor Browser and connecting to the internet without using Tor, thus de-anonymizing you. Protection against these attacks are only possible right now with the `--firewall` flag.

2. Bridges and pluggable transports do not work.

## The Bubblewrap Sandbox

The chroot jail is sandboxed by bubblewrap. This uses Linux namespaces to isolate programs. The actual sandbox is below.

```
sudo bwrap \
--ro-bind /usr/bin/chroot /usr/bin/chroot \
--ro-bind /usr/lib /usr/lib \
--symlink /usr/lib /lib \
--symlink /usr/lib /lib64 \
--dev-bind /tmp/torbrowser-sandbox /tmp/torbrowser-sandbox \
--ro-bind /tmp/torbrowser-sandbox/etc/ /tmp/torbrowser-sandbox/etc/ \
--ro-bind /tmp/torbrowser-sandbox/var/ /tmp/torbrowser-sandbox/var/ \
--ro-bind /tmp/torbrowser-sandbox/usr/ /tmp/torbrowser-sandbox/usr/ \
--ro-bind /tmp/torbrowser-sandbox/bin/ /tmp/torbrowser-sandbox/bin/ \
--ro-bind /tmp/torbrowser-sandbox/lib/ /tmp/torbrowser-sandbox/lib/ \
--ro-bind /tmp/torbrowser-sandbox/lib64/ /tmp/torbrowser-sandbox/lib64/ \
--ro-bind /tmp/torbrowser-sandbox/run/ /tmp/torbrowser-sandbox/run/ \
--ro-bind /tmp/torbrowser-sandbox/mnt/ /tmp/torbrowser-sandbox/mnt/ \
--ro-bind /tmp/torbrowser-sandbox/boot/ /tmp/torbrowser-sandbox/boot/ \
--ro-bind /tmp/torbrowser-sandbox/proc/ /tmp/torbrowser-sandbox/proc/ \
--ro-bind /tmp/torbrowser-sandbox/srv/ /tmp/torbrowser-sandbox/srv/ \
--ro-bind /tmp/torbrowser-sandbox/tmp/ /tmp/torbrowser-sandbox/tmp/ \
--ro-bind /tmp/torbrowser-sandbox/root/ /tmp/torbrowser-sandbox/root/ \
--ro-bind /tmp/torbrowser-sandbox/sys/ /tmp/torbrowser-sandbox/sys/ \
--ro-bind /tmp/torbrowser-sandbox/opt/ /tmp/torbrowser-sandbox/opt/ \
--ro-bind /tmp/torbrowser-sandbox/home/ /tmp/torbrowser-sandbox/home/ \
--bind /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/ /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/ \
--unshare-pid \
--unshare-uts \
--hostname host \
--unshare-cgroup \
--unshare-ipc \
--proc /tmp/torbrowser-sandbox/proc/ \
--tmpfs /tmp/torbrowser-sandbox/tmp/ \
--dev /dev/ \
--new-session \
--die-with-parent \
--as-pid-1 \
--unsetenv XAUTHORITY \
--unsetenv SUDO_COMMAND \
--unsetenv SUDO_USER \
--unsetenv SUDO_UID \
--unsetenv SUDO_GID \
--setenv DISPLAY ${RANDOM} \
--setenv HOME /home/user \
--setenv SHELL /bin/false \
--setenv USER user \
--setenv LOGNAME user \
--setenv LANG en_US.UTF-8 \
--cap-drop all \
--cap-add CAP_SYS_CHROOT \
--cap-add CAP_SETUID \
--cap-add CAP_SETGID \
chroot --userspec=user /tmp/torbrowser-sandbox/
```

The `--ro-bind /tmp/torbrowser-sandbox/*/ /tmp/torbrowser-sandbox/*/` flags mount the entire filesystem in the chroot jail as read-only except /tmp/torbrowser-sandbox/home/user/tor-browser_en-US/ and /dev. This makes it impossible for any malware or user to change anything in the chroot jail except a few Tor Browser parts.

`--unshare-pid` creates a PID namespace. This prevents anything in the sandbox from seeing processes outside of it.

`--unshare-uts` creates a UTS namespace. This prevents the sandbox from getting your hostname.

`--hostname host` sets the hostname to 'host'. This makes sure you cannot be identified by your hostname.

`--unshare-cgroup` creates a cgroup namespace.

`--unshare-ipc` creates a IPC namespace. This prevents anything in the sandbox from communicating with processes outside of it.

`--proc /tmp/torbrowser-sandbox/proc/` mounts /tmp/torbrowser-sandbox/proc/ as the procfs.

`--tmpfs /tmp/torbrowser-sandbox/tmp/` mounts /tmp/torbrowser-sandbox/tmp/ as a tmpfs.

`--dev /dev` mounts /dev as a devtmpfs.

`--new-session` creates a new terminal session for the sandbox. This prevents the sandbox from injecting input into the controlling terminal.

`--die-with-parent` makes the child process die as soon as the parent is killed. This means that if `bwrap` is killed then so will the chroot.

`--as-pid-1` makes whatever program is run in the sandbox PID 1. This prevents an attacker from gaining more information from the host by looking in /proc/1/. It also prevents an attacker with root privileges from escaping the sandbox by chrooting into /proc/1/root/.

`--unsetenv XAUTHORITY` this unsets the XAUTHORITY variable which leaks your username which could be used to identify you. The XAUTHORITY variable is usually set to the .Xauthority file in your home directory. So, if this wasn't unset, it would say `/home/madaidan/.Xauthority` which leaks my username.

`--unsetenv SUDO_COMMAND` this unsets the SUDO_COMMAND variable which says what command created the chroot. This can tell an attacker what to exploit. e.g. if they have an exploit for bubblewrap, they know now to use that.

`--unsetenv SUDO_USER` unsets the SUDO\_USER variable which leaks your username. The SUDO_USER variable tells you what user ran the `sudo` command and as this script runs `sudo`, it leaks your username. This could be used to identify you.

`--unsetenv SUDO_UID` unsets the SUDO\_UID variable which says the UID of the user who ran the sudo command. This may be suspicious to an attacker as the variable will be set even if no sudo command was run inside the sandbox.  The same applies for the `--unsetenv SUDO_GID` part.

`--setenv DISPLAY ${RANDOM}` sets the DISPLAY variable to something random. This prevents a random program from accessing your host's X server.

`--setenv HOME /home/user` sets the HOME variable to the user's home directory.

`--setenv SHELL /bin/false` sets the SHELL variable to /bin/false which makes it harder to get shell access.

`--setenv USER user` and `--setenv LOGNAME user` make sure that the username inside the sandbox is `user`.

`--setenv LANG en_US.UTF-8` sets the default locale in the sandbox to en_US to prevent you from being identified by your locale. If this wasn't set then an attacker could check the system locale and that may give them a hint to where you are located.

`--cap-drop all` drops all capabilities from the sandbox. Capabilities can be used to restrict what root processes can use and as `chroot` needs to be run as root, it is best to restrict it as much as possible.

`--cap-add CAP_SYS_CHROOT` gives the sandbox the CAP\_SYS\_CHROOT capability which is needed to enter the sandbox with `chroot`.

`--cap-add CAP_SETUID` gives the sandbox the CAP\_SETUID capability which is needed to change the user ID.

`--cap-add CAP_SETGID` gives the sandbox the CAP\_SETGID capability which is needed to change the group ID.

The user or the Tor Browser do not get the capabilities given by bubblewrap. Those are only given to the chroot process and root user in the sandbox.

Bubblewrap also uses a mount namespace by default.

And finally,

`chroot --userspec=user /tmp/torbrowser-sandbox/` chroots into /tmp/torbrowser-sandbox as the user 'user'. The user is created just before chrooting by adding a new line to /etc/passwd.

## Other Security/Privacy/Anonymity Features

This script does other things to protect your privacy and increase security.

It anonymizes machine-ids by settings them to `b08dfa6083e7567a1921a715000001fb` which is the same machine-id that [Whonix and Tails uses](https://github.com/Whonix/anon-base-files).

/sys and /proc (from your host) are not mounted. This prevents an attacker from seeing processes outside of the chroot, reduces the attack surface and prevents an attacker from learning more about your hardware which could be used as an identifier.

It prevents certain binaries from being executed by setting their file permissions to `000`. This prevents any user, including root from executing these. The root user could still change the permission back, however.

`su` is restricted because it can be used to gain root privileges. Although it should be impossible to use `su` to gain root as the root user is locked, it's better to be safe.

`rm` is restricted because it can be used to delete important files e.g. the Tor Browser.

`chmod` is restricted because it can be used to revert these permissions. It can still be bypassed by creating a custom chmod program by using the chmod() syscall. This will be fixed once the bubblewrap seccomp filter is implemented.

`chown` is restricted because it can be used to change the ownership of certain files. It can still be bypassed by using the chown() syscall.

`chroot` is restricted because it can be used to break out of the chroot jail. It can still be bypassed by using the chroot() syscall.

`su` is replaced with a dummy binary that just runs `exit 0`. This prevents an attacker from using `su` to gain root.

The root account is locked so it is impossible to login to it.

The DNS server in /etc/resolv.conf (in the chroot) is set to 0.0.0.0. This prevents some clearnet leaks but an attacker could just use IP addresses rather than hostnames to connect to a server. This works because the Tor Browser uses IP addresses to connect to the Tor network so no DNS is needed. It then uses the exit node's DNS for everything else.

Setuid and setgid are removed from binaries in the sandbox. This helps prevent privilege escalation.

Stronger UNIX filesystem permissions (DAC) are configured inside the sandbox.

## Aren't Chroot Escapes Easy to Do?

Yes. Chroot escapes are easy and chroots are not meant for security but when you use an unprivileged user in a chroot and sandbox the chroot then chroot escapes get a lot harder. The Tor Browser is also restricted by AppArmor so the chances of an attacker even getting outside of the browser is unlikely.

This sandbox protects against many chroot escapes such as going into /proc/1/root/, chrooting to parent folders until you get to the actual root directory, ptracing other running processes etc. I have not yet found a chroot escape that works against this sandbox.

Any hardening tips for chroots are welcome.

## Warnings

This is not invincible. While this is secure, nothing is completely secure against all attacks. Attacks known or unknown can break out of this. I try to reduce the risk of this happening as much as possible. As stated in the limitations section, the security of this depends on the integrity of your host OS.

This is not a silver bullet. This does not magically secure all your online activites. You will need to follow OPSEC along with this script to preserve your anonymity.

If you are finished with the Tor Browser then DO NOT quit the script by pressing Ctrl + C while in the terminal, closing the terminal tab etc. Just close the Tor Browser by tapping the X at the top right corner. The script has lots of cleaning up to do after the Tor Browser has closed. Closing it in other methods will terminate the script and prevent other important parts from running.

## Recommendations

The easiest way to break out of this sandbox is by attacking the kernel so it is highly recommended that you use a hardened kernel such as [linux-hardened](https://github.com/anthraxx/linux-hardened).
